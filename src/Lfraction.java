import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      Lfraction f1 = new Lfraction(0, 1);
      Lfraction f2 = new Lfraction(0, 3);
      Lfraction dif = toLfraction(-3.0, 1);
      System.out.println(dif);
   }

   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {

      if(b == 0) throw new IllegalArgumentException("Illegal argument, denominator cannot be 0");

      b = Math.abs(b);

      long divisor = Math.abs(gcd(a, b));

      numerator = a / divisor;
      denominator = b / divisor;
   }

   /** Recursive method to find greatest common divisor.
    * https://stackoverflow.com/questions/6618994/simplifying-fractions-in-java
    * @return divisor
    */
   private static long gcd(long a, long b){
      return b == 0 ? a : gcd(b, a % b);
   }

   /** Public method to access the numerator field.
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field.
    * @return denominator
    */
   public long getDenominator() {
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return String.format("%s/%s", numerator, denominator);
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      if(m == null) {
         return false;
      }
      if(!(m instanceof Lfraction)) {
         return false;
      }

      return ((Lfraction) m).getNumerator() == numerator && ((Lfraction) m).getDenominator() == denominator;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(numerator, denominator);
   }

   /**
    *
    * @param n the exponent
    * @return calculated number raise to the power of some other number
    */
   public Lfraction pow(int n) {
      if (n == 0) {
         return new Lfraction(1,1);
      }
      if (n == 1) {
         return this;
      }
      if (n == -1) {
         return this.inverse();
      }
      if (n > 1) {
         return this.times(this.pow(n-1));
      } else {
         return this.pow(Math.abs(n)).inverse();
      }
   }

   /** Sum of fractions. https://stackoverflow.com/questions/26812131/how-to-add-fractions-in-java
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long newNumerator = (numerator * m.getDenominator()) + (m.getNumerator() * denominator);
      long newDenominator = denominator * m.getDenominator();

      return new Lfraction(newNumerator, newDenominator);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long newNumerator = numerator * m.getNumerator();
      long newDenominator = denominator * m.getDenominator();

      return new Lfraction(newNumerator, newDenominator);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (numerator == 0) throw new IllegalArgumentException("Illegal argument, denominator cannot be 0");
      if (numerator < 0) return new Lfraction(-denominator, numerator);
      return new Lfraction(denominator, numerator);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-numerator, denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      long newNumerator = (numerator * m.getDenominator()) - (denominator * m.getNumerator());
      long newDenominator = denominator * m.getDenominator();

      return new Lfraction(newNumerator, newDenominator);
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {

      if (denominator == 0 || m.denominator == 0) throw new IllegalArgumentException
              ("Illegal argument, denominator cannot be 0");

      long newNumerator = Math.abs(numerator * m.getDenominator());
      long newDenominator = denominator * m.getNumerator();

      if((numerator > 0 && m.getNumerator() < 0) || (numerator < 0 && m.getNumerator() > 0)) {
         newNumerator = -newNumerator;
      }

      return new Lfraction(newNumerator, newDenominator);
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      long delta = (numerator * m.getDenominator()) - (denominator * m.getNumerator());

      if (delta == 0) return 0;
      if (delta > 0) return 1;
      else return -1;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(numerator, denominator);
   }

   /** Integer part of the (improper) fraction.
    * @return integer part of this fraction
    */
   public long integerPart() {
      return numerator / denominator;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      long newNumerator = numerator % denominator;

      return new Lfraction(newNumerator, denominator);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      if (denominator == 0) throw new IllegalArgumentException("denominator cannot be 0");
      return (double)numerator / denominator;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      if (d == 0) throw new IllegalArgumentException("Illegal argument, denominator cannot be 0");
      long newNominator = Math.round(f * d);
      return new Lfraction(newNominator, d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {

      String[] nums = s.split("/");
      long newNumerator;
      long newDenominator;

      if (nums.length != 2) throw new IllegalArgumentException("Illegal fraction: " + s + "." +
              " Should be in 'number/number' format");

      try {
         newNumerator = Long.parseLong(nums[0]);
         newDenominator = Long.parseLong(nums[1]);
      } catch (NumberFormatException e){
         throw new NumberFormatException("Illegal element: " + s + ". Should represent number format");
      }


      return new Lfraction(newNumerator, newDenominator);
   }
}